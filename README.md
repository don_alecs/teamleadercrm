** Setup **

The code cotains a very basic RestfulAPI that calculates discounts for orders.

In order to be able to run this service you will need to install phalcon. 
You can find more information here: https://phalconphp.com/en/download/linux

After you have managed to install the framework, copy the files on you're server and setup a local domain (Ex: discountApi.dev).

Now you can send the orders at ** discountApi.dev/getDiscount **

** Example of an API response **

***
{

  "discountType": "free item",
  
  "discountRule": "For every product of category \"Switches\", when you buy five you get a sixth for free",
  
  "discountValue": "4.99",
  
  "freeItems": [{
    
      "product-id": "B102",
      "quantity": 1,
      "unit-price": "4.99",
      "total": 0 
    }],
  "success": true
  
}

***

The Discount Service give's you information about the discount value that you have to substract from your total order, and if it is the case, a list of free items that you must give to your customers.

***




