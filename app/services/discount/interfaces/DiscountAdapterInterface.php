<?php
namespace Services\Discount\Interfaces;

use Services\Discount\Entity\DiscountResponseEntity;

/**
 * Interface DiscountAdapterInterface
 *
 * @package Services\Discount\Interfaces
 */
interface DiscountAdapterInterface
{

    function execute();


    function discountIsAllowed();

    /**
     * @param DiscountResponseEntity $discount
     *
     * @return mixed
     */
    function applyDiscount(DiscountResponseEntity $discount);

}
