<?php

namespace Services\Discount;

use Services\Customer\CustomerService;
use Services\Discount\Entity\DiscountResponseEntity;
use Services\Discount\Interfaces\DiscountAdapterInterface;
use Services\Product\ProductService;

/**
 * Class DiscountService
 *
 * @package Services\Discount
 */
class DiscountService
{
    /** @var array */
    public $availableDiscounts;

    /** @var ProductService */
    public $productService;

    /** @var CustomerService */
    public $customerService;

    /**
     * DiscountService constructor.
     *
     * @param array           $availableDiscounts
     * @param ProductService  $productService
     * @param CustomerService $customerService
     */
    function __construct(array $availableDiscounts, ProductService $productService, CustomerService $customerService)
    {
        $this->availableDiscounts = $availableDiscounts;
        $this->productService = $productService;
        $this->customerService = $customerService;
    }

    /**
     * If the order is eligible for a discount, it returns the discount details
     *
     * @param $order
     *
     * @return DiscountResponseEntity
     */
    public function getDiscount($order)
    {
        $discount = "";

        foreach ($this->availableDiscounts as $discountAdapter) {
            /** @var DiscountAdapterInterface $adapter */
            $adapter = new $discountAdapter($order, $this->productService, $this->customerService);

            /** @var DiscountResponseEntity $discount */
            $discount = $adapter->execute();

            if ($discount->getSuccess()) {
                break;
            }
        }

        return $discount;
    }
}