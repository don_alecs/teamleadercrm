<?php

namespace Services\Discount\Entity;

/**
 * Class DiscountResponseEntity
 */
class DiscountResponseEntity {

    public $discountType;

    public $discountRule;

    public $discountValue;

    public $freeItems;

    public $success;

    /**
     * @return mixed
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * @param mixed $discountType
     *
     * @return DiscountResponseEntity
     */
    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscountRule()
    {
        return $this->discountRule;
    }

    /**
     * @param mixed $discountRule
     *
     * @return DiscountResponseEntity
     */
    public function setDiscountRule($discountRule)
    {
        $this->discountRule = $discountRule;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * @param mixed $discountValue
     *
     * @return DiscountResponseEntity
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFreeItems()
    {
        return $this->freeItems;
    }

    /**
     * @param mixed $freeItems
     *
     * @return DiscountResponseEntity
     */
    public function setFreeItems($freeItems)
    {
        $this->freeItems = $freeItems;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param mixed $success
     *
     * @return DiscountResponseEntity
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

}