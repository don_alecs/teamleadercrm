<?php

namespace Services\Discount\Adapter;

use Services\Discount\Entity\DiscountResponseEntity;
use Services\Discount\Interfaces\DiscountAdapterInterface;
use Services\Customer\CustomerService;
use Services\Product\ProductService;

/**
 * Class ToolsDiscountAdapter
 *
 * @package Services\Discount\Adapter
 */
class ToolsDiscountAdapter implements DiscountAdapterInterface
{
    /** @var int */
    static $discountPercentage = 20;

    /** @var string */
    static $discountType = 'cash discount';

    /** @var int */
    static $discountCategory = 1;

    /** @var int */
    static $minimumQuantity = 2;

    /** @var int */
    static $minValue = 1000000000000000;

    /** @var string */
    static $discountRule = 'If you buy two or more products of category "Tools" you get 20% discount on the cheapest product';

    /** @var object */
    public $order;

    /** @var ProductService */
    public $productService;

    /** @var CustomerService */
    public $customerService;

    /**
     * VipCustomerDiscountAdapter constructor.
     *
     * @param \stdClass       $order
     * @param ProductService  $productService
     * @param CustomerService $customerService
     */
    public function __construct($order, ProductService $productService, CustomerService $customerService)
    {
        $this->order = $order;
        $this->productService = $productService;
        $this->customerService = $customerService;
    }

    /**
     * Starts the process of discount
     *
     * @return mixed
     */
    public function execute()
    {
        $discount = new DiscountResponseEntity();
        $discount->setSuccess(false);

        $discountIsAllowed = $this->discountIsAllowed();

        if ($discountIsAllowed) {
            $discount = $this->applyDiscount($discount);
        }

        return $discount;
    }

    /**
     * Checks to see if the provided order is qualifing for this discount type
     *
     * @return bool
     */
    public function discountIsAllowed()
    {
        $discountIsAllowed = false;

        if (!isset($this->order->items)) {
            return $discountIsAllowed;
        }

        $quantity = 0;
        foreach ($this->order->items as $product) {

            if(!isset($product->{'product-id'})) {
                continue;
            }

            $productInfo = $this->productService->findById($product->{'product-id'});

            if ($productInfo->category == self::$discountCategory) {
                $quantity = $quantity + $product->quantity;
            }
        }

        if ($quantity >= self::$minimumQuantity) {
            $discountIsAllowed = true;
        }

        return $discountIsAllowed;
    }

    /**
     * We calculate the discount value
     *
     * @param DiscountResponseEntity $discount
     *
     * @return DiscountResponseEntity
     */
    public function applyDiscount(DiscountResponseEntity $discount)
    {
        $cheapestProductValue = $this->getCheapestProductValue();
        $discountValue = round((($cheapestProductValue * self::$discountPercentage) / 100),2);

        $discount->setDiscountType(self::$discountType);
        $discount->setDiscountRule(self::$discountRule);
        $discount->setSuccess(true);
        $discount->setDiscountValue($discountValue);

        return $discount;
    }

    /**
     * Returns the cheapest product value from a receive order
     *
     * @return int
     */
    private function getCheapestProductValue()
    {
        $productValue = self::$minValue;
        foreach ($this->order->items as $product) {
            if ($product->total < $productValue) {
               $productValue = $product->total;
            }
        }

        return $productValue;
    }

}