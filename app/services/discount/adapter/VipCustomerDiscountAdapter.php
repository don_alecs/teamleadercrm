<?php

namespace Services\Discount\Adapter;

use Services\Customer\CustomerService;
use Services\Discount\Interfaces\DiscountAdapterInterface;
use Services\Discount\Entity\DiscountResponseEntity;
use Services\Product\ProductService;

/**
 * Class HoleOrderDiscountAdapter
 *
 * @package Services\Discount\Adapter
 */
class VipCustomerDiscountAdapter implements DiscountAdapterInterface
{
    /** @var int */
    static $minValueForDiscount = 1000;

    /** @var int */
    static $discountPercentage = 10;

    /** @var string  */
    static $discountType = 'cash discount';

    /** @var string  */
    static $discountRule = 'If a customer has a purchase history grater then 1000 E,he gets 10% discount on the whole order';

    /** @var object */
    public $order;

    /** @var ProductService  */
    public $productService;

    /** @var CustomerService  */
    public $customerService;

    /**
     * VipCustomerDiscountAdapter constructor.
     *
     * @param \stdClass                $order
     * @param ProductService  $productService
     * @param CustomerService $customerService
     */
    public function __construct($order, ProductService $productService, CustomerService $customerService)
    {
        $this->order = $order;
        $this->productService = $productService;
        $this->customerService = $customerService;
    }

    /**
     * Starts the process of discount
     *
     * @return mixed
     */
    public function execute()
    {
        $discount = new DiscountResponseEntity();
        $discount->setSuccess(false);

        $discountIsAllowed = $this->discountIsAllowed();

        if ($discountIsAllowed) {
            $discount = $this->applyDiscount($discount);
        }

        return $discount;
    }

    /**
     * Checks to see if the provided order is qualifing for this discount type
     *
     * @return bool
     */
    public function discountIsAllowed()
    {
        $discountIsAllowed = false;

        if (!isset($this->order->{'customer-id'})) {
            return $discountIsAllowed;
        }

        $customerInfo = $this->customerService->findById($this->order->{'customer-id'});

        if($customerInfo !== false && $customerInfo->revenue >= self::$minValueForDiscount) {
            $discountIsAllowed = true;
        }

        return $discountIsAllowed;
    }

    /**
     * We calculate the discount value
     *
     * @param DiscountResponseEntity $discount
     *
     * @return DiscountResponseEntity
     */
    public function applyDiscount(DiscountResponseEntity $discount)
    {
        $discountValue = (($this->order->total * self::$discountPercentage)/100);

        $discount->setDiscountType(self::$discountType);
        $discount->setDiscountRule(self::$discountRule);
        $discount->setSuccess(true);
        $discount->setDiscountValue($discountValue);

        return $discount;
    }

}