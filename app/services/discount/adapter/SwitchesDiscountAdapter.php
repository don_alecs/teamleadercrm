<?php

namespace Services\Discount\Adapter;

use Services\Discount\Entity\DiscountResponseEntity;
use Services\Discount\Interfaces\DiscountAdapterInterface;
use Services\Customer\CustomerService;
use Services\Product\ProductService;

/**
 * Class SwitchesDiscountAdapter
 *
 * @package Services\Discount\Adapter
 */
class SwitchesDiscountAdapter implements DiscountAdapterInterface
{
    /** @var string */
    static $discountType = 'free item';

    /** @var int */
    static $discountCategory = 2;

    /** @var int */
    static $minimumQuantity = 5;

    /** @var int */
    static $freeItemQuantity = 1;

    /** @var int */
    static $freeItemTotalCost = 0;

    /** @var string */
    static $discountRule = 'For every product of category "Switches", when you buy five you get a sixth for free';

    /** @var object */
    public $order;

    /** @var ProductService */
    public $productService;

    /** @var CustomerService */
    public $customerService;

    /**
     * SwitchesDiscountAdapter constructor.
     *
     * @param                 $order
     * @param ProductService  $productService
     * @param CustomerService $customerService
     */
    public function __construct($order, ProductService $productService, CustomerService $customerService)
    {
        $this->order = $order;
        $this->productService = $productService;
        $this->customerService = $customerService;
    }

    /**
     * @return mixed
     */
    function execute()
    {
        $discount = new DiscountResponseEntity();
        $discount->setSuccess(false);

        $discountIsAllowed = $this->discountIsAllowed();

        if ($discountIsAllowed) {
            $discount = $this->applyDiscount($discount);
        }

        return $discount;
    }

    /**
     * Checks to see if the provided order is qualifing for this discount type
     *
     * @return bool
     */
    public function discountIsAllowed()
    {
        $discountIsAllowed = false;

        if (!isset($this->order->items)) {
            return $discountIsAllowed;
        }

        foreach ($this->order->items as $product) {
            if(!isset($product->{'product-id'})) {
                continue;
            }

            $productInfo = $this->productService->findById($product->{'product-id'});

            if ($productInfo->category == self::$discountCategory && $product->quantity >= self::$minimumQuantity) {
                $discountIsAllowed = true;

                break;
            }
        }

        return $discountIsAllowed;
    }

    /**
     * We calculate the discount value
     *
     * @param DiscountResponseEntity $discount
     *
     * @return DiscountResponseEntity
     */
    public function applyDiscount(DiscountResponseEntity $discount)
    {
        $freeItems = [];
        $discountValue = 0;

        foreach ($this->order->items as $product) {
            $productInfo = $this->productService->findById($product->{'product-id'});
            if ($productInfo->category == self::$discountCategory && $product->quantity >= self::$minimumQuantity) {

                if ($product->quantity == self::$minimumQuantity) {
                    $freeItem = $product;
                    $freeItem->quantity = self::$freeItemQuantity;
                    $freeItem->discountValue = $freeItem->{'unit-price'};
                    $freeItem->total = self::$freeItemTotalCost;

                    array_push($freeItems, $freeItem);
                }

                if ($product->quantity > self::$minimumQuantity) {
                    $discountValue = $product->{"unit-price"};
                }

            }
        }

        $discount->setDiscountType(self::$discountType);
        $discount->setDiscountRule(self::$discountRule);
        $discount->setSuccess(true);
        $discount->setFreeItems($freeItems);
        $discount->setDiscountValue($discountValue);

        return $discount;
    }

}