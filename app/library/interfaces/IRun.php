<?php

namespace Interfaces;

interface IRun {

	/**
	 * Main method to run the object
	 */
	public function run();
}
