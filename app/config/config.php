<?php
/**
 * Settings to be stored in dependency injector
 */

$settings = [
    'services' => [
        'discountService' => '\Services\Discount\DiscountService',
    ],
    'availableDiscounts' => [
        'Services\Discount\Adapter\VipCustomerDiscountAdapter',
        'Services\Discount\Adapter\SwitchesDiscountAdapter',
        'Services\Discount\Adapter\ToolsDiscountAdapter'
    ],
    'authConfig' => [
        'secretKey' => '!345#dsaa1<>23gh'
    ]
];

return $settings;
