<?php

$routes[] = [
    'method' => 'post',
    'route' => '/getDiscount',
    'handler' => ['Controllers\DiscountController', 'getDiscountAction']
];

return $routes;
