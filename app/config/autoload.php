<?php

/**
 * Auto Load Class files by namespace
 *
 * @eg 
 	'namespace' => '/path/to/dir'
 */

$autoload = [
	'Application' => APP_DIR . '/library/application/',
	'Interfaces' => APP_DIR . '/library/interfaces/',
	'Controllers' => APP_DIR . '/controllers/',
	'Service' => APP_DIR .'/services/',
];

return $autoload;
