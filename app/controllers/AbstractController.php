<?php

namespace Controllers;

use Phalcon\Http\Request;

/**
 * Class AbstractController
 *
 * @package Controllers
 */
class AbstractController extends \Phalcon\Mvc\Controller
{
    public $request;

    public $requestHeaders;

    public function onConstruct()
    {
        $request     = new Request();


        $this->requestHeaders = $this->populateHeaders($request);
        $this->request        = $this->populateRequestData($request);
    }

    /**
     * Populates the headers with the right information
     *
     * @param $request
     *
     * @return \stdClass
     */
    public function populateHeaders($request)
    {
        $headers = new \stdClass();

        if ($request->getHeader('Authentication') != null) {
            $headers->authentication = $request->getHeader('Authentication');
        }

        return $headers;
    }

    /**
     * @param $request
     *
     * @return \stdClass
     */
    public function populateRequestData($request)
    {
        $requestData = new \stdClass();

        switch($request->getContentType()) {
            case "application/json;charset=UTF-8":
            case "application/json":
                $requestData->data = json_decode($request->getRawBody());
                break;

            default:
                if ($request->isPost()) {
                    $requestData->data = $request->getPost();
                }

                if ($request->isGet()) {
                    $requestData->data = $request->getQuery();
                }

        }

        return $requestData;
    }
}