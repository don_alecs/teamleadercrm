<?php
namespace Controllers;

use \Phalcon\Di;
use Services\Discount\DiscountService;

/**
 * Class DiscountController
 *
 * @package Controllers
 */
class DiscountController extends AbstractController
{

    /** @var DiscountService */
    public $discountService;

    public function onConstruct()
    {
        parent::onConstruct();

        $this->discountService = $this->di->getShared("discountService");
    }

    public function getDiscountAction()
    {
        $order = $this->request->data;
        $discount = $this->discountService->getDiscount($order);

        echo json_encode($discount);
    }
}