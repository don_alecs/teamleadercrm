<?php
define ("APP_DIR", dirname(__DIR__) . "/app");
define ("CONFIG_DIR", APP_DIR . "/config");

require APP_DIR . '/library/interfaces/IRun.php';
require APP_DIR . '/library/application/Micro.php';

$autoload = CONFIG_DIR . '/autoload.php';
$config = CONFIG_DIR . '/config.php';
$routes =  CONFIG_DIR . '/routes.php';

try {
    $app = new Application\Micro();
    $loader = $app->setAutoload($autoload);
    $app->setConfig($config);

    // Here it should be the authentication mechanism
    // Get Authorization and checkit if is ok .
    // $app->request->getHeader("Authorization");
    $app->setRoutes($routes);
    $app->run();
} catch (Exception $e) {
    $app->response->setStatusCode(500, "Server Error");
    $app->response->setContent($e->getMessage());
    $app->response->send();
}